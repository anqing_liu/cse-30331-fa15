Homework 1 - Part A
===================
Due 2015/09/01 at the beginning of class.

Q1. Fork and clone this repository according to the instructions at
[the repository root](https://bitbucket.org/CSE-30331-FA15/cse-30331-fa15). 
Note: in order to get credit for this question, your forked repository
must pass the checks in step 4.

*Please fill in the following so we know who you are:*

- Your name:
- Your Notre Dame NetID:

Q2. A class that uses dynamically allocated memory should always
include what three member functions (methods)? Why?

*Fill in your answer here.*

To submit, do:

    git commit -am 'Submit Homework 1A'
    git push

